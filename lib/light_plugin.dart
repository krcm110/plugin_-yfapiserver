
import 'light_plugin_platform_interface.dart';

class LightPlugin {
  Future<String?> getPlatformVersion() {
    return LightPluginPlatform.instance.getPlatformVersion();
  }
  Future<String?> closeLight() {
    return LightPluginPlatform.instance.closeLight();
  }
  Future<String?> openLight() {
    return LightPluginPlatform.instance.openLight();
  }
}
