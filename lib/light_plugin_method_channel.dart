import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'light_plugin_platform_interface.dart';

/// An implementation of [LightPluginPlatform] that uses method channels.
class MethodChannelLightPlugin extends LightPluginPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('light_plugin');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
  @override
  Future<String?> openLight() async {
    final version = await methodChannel.invokeMethod<String>('openLight');
    return version;
  }
  @override
  Future<String?> closeLight() async {
    final version = await methodChannel.invokeMethod<String>('closeLight');
    return version;
  }
}
