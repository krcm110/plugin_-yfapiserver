package com.example.light_plugin;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;

import com.yf.apiserver.IYFAPIserver;

import org.jetbrains.annotations.NotNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import static android.content.Context.BIND_AUTO_CREATE;

/** LightPlugin */
public class LightPlugin extends FlutterActivity implements FlutterPlugin, MethodCallHandler, ActivityAware {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  private ActivityPluginBinding activityBinding;
  private IYFAPIserver apiserver;
  static boolean server_flag = false;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    System.out.println("====onAttachedToEngine!");
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "light_plugin");
    channel.setMethodCallHandler(this);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    System.out.println("====onMethodCall!");
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    }else if(call.method.equals("openLight")){
      String resultOpen = "初始化失败";
      if(server_flag){
        try{
          apiserver.yfSetLCDOn();
        }catch (RemoteException e){
        }
        resultOpen="初始化成功";
      }else{

        resultOpen ="初始化失败";
      }
      result.success(resultOpen);
    } else if(call.method.equals("closeLight")){
      String resultOpen = "初始化失败";
      if(server_flag){
        try{
          apiserver.yfSetLCDOff();
        }catch (RemoteException e){
        }
        resultOpen="初始化成功";
      }else{
        resultOpen ="初始化失败";
      }
      result.success(resultOpen);
    }else {
      result.notImplemented();
    }
  }

  public void initserver(){//初始化服务
    System.out.println("====我开始初始化!");
    Intent intent = new Intent(new Intent("com.youngfeel.YFAPIServer"));
    intent.setPackage("com.yf.apiserver");
    activityBinding.getActivity().startService(intent);
    activityBinding.getActivity().bindService(intent, new ServiceConnection() {
      @Override
      public void onServiceConnected(ComponentName name, IBinder service)
      {
        apiserver = IYFAPIserver.Stub.asInterface(service);
        System.out.println("====service connected!");
        server_flag = true;
      }

      @Override
      public void onServiceDisconnected(ComponentName name)
      {
        System.out.println("====service disconnected!");
        server_flag = false;
      }
    }, BIND_AUTO_CREATE);
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    System.out.println("====onDetachedFromEngine!");
    channel.setMethodCallHandler(null);
  }

  @Override
  public void onAttachedToActivity(@NonNull @NotNull ActivityPluginBinding binding) {
    System.out.println("=====onAttachedToActivity");
    activityBinding = binding;
    initserver();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {

  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull @NotNull ActivityPluginBinding binding) {
    System.out.println("=====onReattachedToActivityForConfigChanges");
    activityBinding = binding;
  }

  @Override
  public void onDetachedFromActivity() {

  }
}
