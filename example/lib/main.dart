import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:light_plugin/light_plugin.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String _result = '';
  final _lightPlugin = LightPlugin();

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion = await _lightPlugin.getPlatformVersion() ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Column(
          children: [
            Text('Running on: $_platformVersion\n'),
            SizedBox(
              height: 20,
            ),
            Text('组件初始化是否成功：$_result'),
            GestureDetector(
              onTap: () async => {
                _result = await _lightPlugin.openLight() ?? "sss",
                _result = ' 开灯$_result',
                setState(() {}),
              },
              child: Container(color: Colors.blue, padding: EdgeInsets.all(20), child: Text('开灯')),
            ),
            SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () async => {
                _result = await _lightPlugin.closeLight() ?? "sss",
                _result = ' 关灯$_result',
                setState(() {}),
              },
              child: Container(color: Colors.blue, padding: EdgeInsets.all(20), child: Text('关灯')),
            ),
          ],
        ),
      ),
    );
  }
}
