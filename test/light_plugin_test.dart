import 'package:flutter_test/flutter_test.dart';
import 'package:light_plugin/light_plugin.dart';
import 'package:light_plugin/light_plugin_platform_interface.dart';
import 'package:light_plugin/light_plugin_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockLightPluginPlatform
    with MockPlatformInterfaceMixin
    implements LightPluginPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final LightPluginPlatform initialPlatform = LightPluginPlatform.instance;

  test('$MethodChannelLightPlugin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelLightPlugin>());
  });

  test('getPlatformVersion', () async {
    LightPlugin lightPlugin = LightPlugin();
    MockLightPluginPlatform fakePlatform = MockLightPluginPlatform();
    LightPluginPlatform.instance = fakePlatform;

    expect(await lightPlugin.getPlatformVersion(), '42');
  });
}
